#include <Adafruit_NeoPixel.h>


#define LED_PIN    4
 

const int LIGHT_DELAY = 200;
const uint8_t LED_COUNT = 9;
uint32_t LEFT_ARROW_COLOR;
uint32_t RIGHT_ARROW_COLOR;
const uint32_t ARRAY_X_LENTGH = 3;
const uint32_t ARRAY_Y_LENTGH = 3;

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip342
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

void setup() {
  Serial.begin(9600);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  strip.setBrightness(255);
  strip.setPixelColor(0, 255, 0, 0);
  strip.show();
  //set_to_white(strip, LED_COUNT);
  LEFT_ARROW_COLOR = strip.Color(255, 0, 0);
  RIGHT_ARROW_COLOR = strip.Color(255, 0, 0);
}

void loop() {
  // put your main code here, to run repeatedly: 
  // set_to_white(strip, LED_COUNT );

  //draw_line(&strip, LED_COUNT, random(0,3), random(0,3) , random(0,3), random(0,3) , random(0,255), random(0,255), random(0,255));
  //set_to_white(&strip, LED_COUNT);
  int r = 255;
  int g = 0;
  int b = 0;
  for (int i = 0 ; i < 3; ++i){
    close_lights(&strip, LED_COUNT);
    draw_line(&strip, LED_COUNT, i, 0 , i, 2, r , g, b);
    strip.show();
    delay(500);
  }
  r = 0;
  g = 255;
   for (int i = 0 ; i < 3; ++i){
    close_lights(&strip, LED_COUNT);
    draw_line(&strip, LED_COUNT, 0, i , 2, i, r , g, b);
    strip.show();
    delay(500);
  }
  g = 0;
  b = 255;
   for (int i = 0 ; i < 3; ++i){
    close_lights(&strip, LED_COUNT);
    draw_line(&strip, LED_COUNT, 0, i , i, 0, r , g, b);
    strip.show();
    delay(500);
  }
  close_lights(&strip, LED_COUNT);
  draw_line(&strip, LED_COUNT, 1, 2 , 2, 1, r , g, b);
  strip.show();
  delay(500);
  close_lights(&strip, LED_COUNT);
  draw_line(&strip, LED_COUNT, 2, 2 , 2, 2, r , g, b);
  strip.show();
  delay(500);
  /*
  delay(5000);
  close_lights(&strip, LED_COUNT);
  for (int i = 0 ; i < 10 ; ++i) {
    int r = 255;
    int g = 0;
    int b = 0;
    //draw_line(&strip, LED_COUNT, 0, 2 , 2, 2, r , g, b);
    draw_line(&strip, LED_COUNT, 2, 0 , 2, 1, r , g, b);
    strip.show();
    delay(1000);
    close_lights(&strip, LED_COUNT);
    delay(1000);
  }
  set_to_white(&strip, LED_COUNT);
  delay(5000);
  close_lights(&strip, LED_COUNT);
  for (int i = 0 ; i < 10 ; ++i) {
   // draw_line(&strip, LED_COUNT, 0, 0 , 0, 2, r , g, b);
    draw_line(&strip, LED_COUNT, 0, 0 , 2, 0, r , g, b);
    strip.show();
    delay(1000);
    close_lights(&strip, LED_COUNT);
    delay(1000);
  }
  */
}



/*
 * set_to_white 
 * 
 * Simple function to set all the leds to white
 */
void set_to_white(Adafruit_NeoPixel *strip, const uint8_t LED_COUNT ) 
{
  for (int i = 0 ; i < LED_COUNT ; ++i)
    {
      strip->setPixelColor(i, 255, 255, 255);
      strip->show();
    }
}

void close_lights(Adafruit_NeoPixel *strip, const uint8_t LED_COUNT ) 
{
  for (int i = 0 ; i < LED_COUNT ; ++i)
    {
      strip->setPixelColor(i, 0, 0, 0);
      strip->show();
    }
}

void draw_line (Adafruit_NeoPixel *strip, const uint8_t LED_COUNT, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t r, uint8_t g, uint8_t b  )
{
  float slope = (y2 - y1) / (x2 - x1);
  
  bool foward = true;
  uint8_t z = 0;
  uint8_t xt, yt;
  for (int i = 0; i < ARRAY_Y_LENTGH ; ++i) 
  {
    xt = i;
    for (int y = 0; y < ARRAY_X_LENTGH; ++y)
    {
      
      if (foward) 
        yt = y;
      else 
        yt =(ARRAY_Y_LENTGH -1 ) - y;
      if (x2-x1 != 0){
        if ((yt - y1) == slope * (xt - x1))
          {
            strip->setPixelColor(z, r, g, b);
            
          } 
      }
      else 
      {
        if ( xt == x1 && (( y1 < y2 )? y1 : y2) <= yt && yt <= (( y1 > y2 )? y1 : y2)) 
        {
          strip->setPixelColor(z, r, g, b);
        }
      }
      z++;
    }
    foward = !foward;
   
  }
}

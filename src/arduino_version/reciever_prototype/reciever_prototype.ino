#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>
#include <Adafruit_NeoPixel.h>


#define LED_PIN    4
 

const int LIGHT_DELAY = 200;
const uint8_t LED_COUNT = 9;
uint32_t LEFT_ARROW_COLOR;
uint32_t RIGHT_ARROW_COLOR;
const uint32_t ARRAY_X_LENTGH = 3;
const uint32_t ARRAY_Y_LENTGH = 3;


Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

const char LEFT_SIGNAL[]="L";
const char RIGHT_SIGNAL[]="R";

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";
void setup() {
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
  //strip.begin();
  //strip.show(); // Initialize all pixels to 'off'
  //strip.setBrightness(25);
  //set_to_white(strip, LED_COUNT);
  LEFT_ARROW_COLOR = strip.Color(255, 0, 0);
  RIGHT_ARROW_COLOR = strip.Color(255, 0, 0);
}
void loop() {
  if (radio.available()) {
    char text[32] = "";
    radio.read(&text, sizeof(text));
    Serial.println(text);
    if (text[0] == LEFT_SIGNAL[0]) {
      Serial.println("Left signal queried");
      //close_lights(strip, LED_COUNT);
      //set_to_white(strip, LED_COUNT);
      Serial.println("Setting to white");
      //draw_line(strip, LED_COUNT, 0, 0, 2 , 2, 255, 0, 0);
      //draw_line(strip, LED_COUNT, 0, 1, 1 , 2, 255, 0, 0);
      //strip.show();
    }
    else if (text[0] == RIGHT_SIGNAL[0]) {
      Serial.println("Right signal queried");
    }
  }
  else
  {
    //Serial.println("Rien a recevoir");
  }
}


/*
 * set_to_white 
 * 
 * Simple function to set all the leds to white
 */
void set_to_white(Adafruit_NeoPixel strip, const uint8_t LED_COUNT ) 
{
  for (int i = 0 ; i < LED_COUNT ; ++i)
    {
      strip.setPixelColor(i, 255, 255, 255);
      strip.show();
    }
}

void close_lights(Adafruit_NeoPixel strip, const uint8_t LED_COUNT ) 
{
  for (int i = 0 ; i < LED_COUNT ; ++i)
    {
      strip.setPixelColor(i, 0, 0, 0);
      strip.show();
    }
}

void draw_line (Adafruit_NeoPixel strip, const uint8_t LED_COUNT, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t r, uint8_t g, uint8_t b  )
{

 float slope = (y2 - y1) / (x2 - x1);
  
  bool foward = true;
  uint8_t z = 0;
  uint8_t xt, yt;
  for (int i = 0; i < ARRAY_Y_LENTGH ; ++i) 
  {
    xt = i;
    for (int y = 0; y < ARRAY_X_LENTGH; ++y)
    {
      
      if (foward) 
        yt = y;
      else 
        yt =(ARRAY_Y_LENTGH -1 ) - y;
      if (x2-x1 != 0){
        if ((yt - y1) == slope * (xt - x1))
          {
            strip->setPixelColor(z, r, g, b);
            
          } 
      }
      else 
      {
        if ( xt == x1 && (( y1 < y2 )? y1 : y2) <= yt && yt <= (( y1 > y2 )? y1 : y2)) 
        {
          strip->setPixelColor(z, r, g, b);
        }
      }
      z++;
    }
    foward = !foward;
   
  }
}

#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

const char LEFT_SIGNAL[]="L";
const char RIGHT_SIGNAL[]="R";
const int LEFT_SIGNAL_PIN=2;
const int RIGHT_SIGNAL_PIN=3;
/*
 * Code du TRANSMITTER
 * 
 *  1. Initialiser un module radio
 *  2. Setter l'adresse de communication
 *  3. Initialiser le module 
 *  4. ouvrir le canal de communication
 *  5. Setter un power amplifier level
 *  6. Comme c'est un transmitter, on ferme l'écoute
 *  7. On definit notre message a envoyer
 *  8. On l'envoie
 *  9. On donne du temps au reciever de lire 
 */

const int CE_PIN = 7;
const int CSN_PIN = 8;

// 1. Initialiser un module radio
// radio(CE, CSN)
RF24 radio(CE_PIN, CSN_PIN);
// 2. Setter l'adresse de communication
const byte adresse[6] = "00001";
const char text[] =  "S";

void setup() {
  
  // 3. Initialiser le module 
  radio.begin();
  // 4. ouvrir le canal de communication
  radio.openWritingPipe(adresse);
  // 5. Setter un power amplifier level
  radio.setPALevel(RF24_PA_MIN);
  // 6. Comme c'est un transmitter, on ferme l'écoute
  radio.stopListening();
  Serial.begin(9600);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
}

void loop() {

  int leftSignal = digitalRead(LEFT_SIGNAL_PIN);
  int rightSignal = digitalRead(RIGHT_SIGNAL_PIN);
 
  if(leftSignal == LOW){
     Serial.println("Left signal");
     radio.write(&LEFT_SIGNAL, sizeof(LEFT_SIGNAL));
     delay(1000);
  }
  else if (rightSignal == LOW){
     Serial.println("Right signal");
     radio.write(&RIGHT_SIGNAL, sizeof(RIGHT_SIGNAL));
     delay(1000);
  }
 
}

    
